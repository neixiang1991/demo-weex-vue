### 步骤

1. 安装weex-cli，创建并启动项目

```
npm install weex-toolkit -g
weex create demo-weex-vue
cd demo-weex-vue
npm start
```

2. 添加iOS平台

```
weex platform add ios
cd platforms/ios
pod install
weex run ios
```

3. 添加安卓平台

```
weex platform add android
cd platforms/android
weex run android
```

4. 账号密码

```
10086
111111
```

### 注意

- iOS执行`pod install`失败

```
// 修改Podfile中
source 'https://mirrors.tuna.tsinghua.edu.cn/git/CocoaPods/Specs.git'
// 修改Podfile.lock中
- WeexSDK (0.28.0)
```

- `weex run ios`失败

```
Xcode12+去掉了xrun instruments命令导致，见https://github.com/weexteam/weex-toolkit/pull/3
目前只能编译后将dist下的js文件，拷贝到platforms/ios/bundlejs文件夹下，再用xCode运行项目
```

- `weex run android`失败

```
weex安卓不支持jdk8以上版本，需要对jdk降级到8
Android Studio需要4.1.3及以下版本才能打开
```

- iOS图片不显示

```
// WXLayer.h中加入
[super display];
```
